package com.example.campusrecruitment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.campusrecruitment.Adapters.ApplicationTabViewAdapter;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

public class Applicatons extends AppCompatActivity {
    SharedPreferences sharedPreferences_for_login;
    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applications);

        final ViewPager viewPager = findViewById(R.id.view_pager);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.addTab(tabs.newTab().setText("Upcoming"));
        tabs.addTab(tabs.newTab().setText("Outdated"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
        ApplicationTabViewAdapter adapter=new ApplicationTabViewAdapter(Applicatons.this,getSupportFragmentManager(),tabs.getTabCount(),sharedPreferences_for_login);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}