package com.example.campusrecruitment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.campusrecruitment.APIs.Api;
import com.example.campusrecruitment.APIs.ApiService;
import com.example.campusrecruitment.ModelClass.City;
import com.example.campusrecruitment.ModelClass.Result;
import com.example.campusrecruitment.ModelClass.State;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Registration extends AppCompatActivity {
    CircleImageView profile_imageview;
    EditText fullname_et, email_et, mobile_et, address_et, college_et, enroll_no_et, password_et, confirm_pass_et;
    Button signup_btn;
    TextView email_tv;
    Spinner city_sp, branch_sp, semester_sp, state_sp, college_sp;
    RadioGroup radioGroup;
    RadioButton gender_rb;
    ArrayList<String> strings_state = new ArrayList<>();
    ArrayList<String> strings_city = new ArrayList<>();
    ArrayList<String> strings_college = new ArrayList<>();
    ArrayList<String> strings_branch = new ArrayList<>();
    ArrayList<String> strings_sem = new ArrayList<>();


    SharedPreferences sharedPreferences_for_login;
    ProgressDialog progressDialog;
    Cursor cursor;

    String typedemail, emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";
    String fname, gender, email, mobile, address, state, city, college, branch, sem, enrollment, password;
    Integer REQUEST_CODE = 200;

    Uri uri;
    String filepath;
    ArrayAdapter<String> state_adapter;
    ArrayAdapter<String> city_adapter;
    ArrayAdapter<String> college_adapter;
    ArrayAdapter<String> sem_adapter;
    ArrayAdapter<String> branch_adapter;

    Retrofit retrofit = new Retrofit.Builder()

            .baseUrl(Api.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        profile_imageview = findViewById(R.id.registration_profile_image);
        profile_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE);
            }
        });


        fullname_et = findViewById(R.id.registration_stud_name);
        radioGroup = findViewById(R.id.registration_gender_rg);
        gender_rb = findViewById(radioGroup.getCheckedRadioButtonId());
        email_et = findViewById(R.id.registration_email);
        email_tv = findViewById(R.id.regist_Email_tv);
        mobile_et = findViewById(R.id.registration_mobile);
        address_et = findViewById(R.id.registration_address);
        state_sp = findViewById(R.id.registration_state_sp);
        city_sp = findViewById(R.id.registration_city_sp);
//        city_sp.setVisibility(View.GONE);
        college_sp = findViewById(R.id.registration_college_sp);
        branch_sp = findViewById(R.id.registration_branch_sp);
        semester_sp = findViewById(R.id.registration_sem_sp);
        enroll_no_et = findViewById(R.id.registration_enrollment_number);
        password_et = findViewById(R.id.registration_password);
        confirm_pass_et = findViewById(R.id.registration_confirm_password);
        signup_btn = findViewById(R.id.registration_signup_btn);

        strings_state.add("Select State");
        strings_city.add("Select City");
        strings_college.add("Select College");
        strings_branch.add("Select Branch");
        strings_sem.add("Select Semester");

        setstate();

        state_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (state_sp.getSelectedItem().toString().equals("Select State")) {
                    city_sp.setVisibility(View.GONE);
                } else {
                    city_sp.setVisibility(View.VISIBLE);
                    setcity(state_sp.getSelectedItem().toString());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

                if (state_sp.getSelectedItem().toString().equals("Select State")) {
                    city_sp.setVisibility(View.GONE);
                } else {
                    city_sp.setVisibility(View.VISIBLE);
                    setcity(state_sp.getSelectedItem().toString());
                }
            }
        });

        email_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                typedemail = email_et.getText().toString().trim();
                if (typedemail.matches(emailPattern) && editable.length() > 0) {

                    email_tv.setText("valid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.green));
                } else {

                    email_tv.setText("invalid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.red));
                }

            }
        });
    }

    public void onclick1(View v) {

        if (fullname_et.length() == 0) {
            fullname_et.setError("enter fullname");
        } else if (email_et.length() == 0) {
            email_et.setError("enter email");
        } else if (email_tv.getText().toString().equals("invalid Email")) {
            email_et.setError("Enter Valid Email address");
        } else if (mobile_et.length() < 10) {
            mobile_et.setError("enter mobile number");
        } else if (address_et.length() == 0) {
            address_et.setError("enter address");
        } else if (state_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Registration.this, "select state", Toast.LENGTH_SHORT).show();
        } else if (city_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Registration.this, "Select City", Toast.LENGTH_SHORT).show();
        } else if (college_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Select College", Toast.LENGTH_SHORT).show();
        } else if (branch_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Registration.this, "Select Branch", Toast.LENGTH_SHORT).show();
        } else if (semester_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Registration.this, "Select Semester", Toast.LENGTH_SHORT).show();
        } else if (enroll_no_et.length() < 12) {
            enroll_no_et.setError("enter enrollment number");
        } else if (password_et.length() < 6) {
            password_et.setError("enter password");
        } else if (confirm_pass_et.length() < 6) {
            confirm_pass_et.setError("enter confirm password");
        } else if (password_et.getText().toString().equals(confirm_pass_et.getText().toString())) {

//            Log.d("joshivishal", "uri>>>> "+uri);
//            filepath=getRealPathFromURIPathString(uri,Registration.this);

            Log.d("joshivishal", "filepath>>>> " + filepath);
            fname = fullname_et.getText().toString();
            gender = gender_rb.getText().toString();
            email = email_et.getText().toString();
            mobile = mobile_et.getText().toString();
            address = address_et.getText().toString();
            state = state_sp.getSelectedItem().toString();
            city = city_sp.getSelectedItem().toString();
            college = college_sp.getSelectedItem().toString();
            branch = branch_sp.getSelectedItem().toString();
            sem = semester_sp.getSelectedItem().toString();
            enrollment = enroll_no_et.getText().toString();
            password = password_et.getText().toString();

            if (filepath == null) {
                Toast.makeText(this, "select profie picture", Toast.LENGTH_SHORT).show();
            } else {
                uploadRegistrationData(filepath, user_type, fname, gender, email, mobile, address, state, city, college, branch, sem, enrollment, password);
            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(Registration.this.getContentResolver(), uri);
                profile_imageview.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            filepath = getRealPathFromURIPathString(uri, Registration.this);

        }
    }

    public void setstate() {

        progressDialog = new ProgressDialog(Registration.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        ApiService service = retrofit.create(ApiService.class);

        Call<State> call = service.getstate(user_type);

        call.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {

                        //set states data
                        strings_state.clear();
                        strings_state.add("Select State");

                        for (int i = 0; i < response.body().getStates().size(); i++) {
                            strings_state.add(response.body().getStates().get(i).getState_name());
                        }
                        state_adapter = new ArrayAdapter<String>(Registration.this, R.layout.spinnn, strings_state);
                        state_sp.setAdapter(state_adapter);
                        state_adapter.notifyDataSetChanged();

                        //set college data
                        strings_college.clear();
                        strings_college.add("Select College");
                        for (int i = 0; i < response.body().getColleges().size(); i++) {
                            strings_college.add(response.body().getColleges().get(i).getCollege_name());
                        }
                        college_adapter = new ArrayAdapter<String>(Registration.this, R.layout.spinnn, strings_college);
                        college_sp.setAdapter(college_adapter);
                        college_adapter.notifyDataSetChanged();

                        //set branch data
                        strings_branch.clear();

                        strings_branch.add("Select Branch");

                        for (int i = 0; i < response.body().getBranches().size(); i++) {
                            strings_branch.add(response.body().getBranches().get(i).getBranch_name());
                        }
                        branch_adapter = new ArrayAdapter<String>(Registration.this, R.layout.spinnn, strings_branch);
                        branch_sp.setAdapter(branch_adapter);
                        branch_adapter.notifyDataSetChanged();

                        //set sem data
                        strings_sem.clear();

                        strings_sem.add("Select Semester");

                        for (int i = 0; i < response.body().getSemesters().size(); i++) {
                            strings_sem.add(response.body().getSemesters().get(i).getSemester_num());
                        }
                        sem_adapter = new ArrayAdapter<String>(Registration.this, R.layout.spinnn, strings_sem);
                        semester_sp.setAdapter(sem_adapter);
                        sem_adapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(Registration.this, "no data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Registration.this, "body null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Registration.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setcity(String state_name) {

        progressDialog = new ProgressDialog(Registration.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

//        Log.d("test.registration.","is.."+state_name);

        ApiService service = retrofit.create(ApiService.class);

        Call<City> call = service.getcity(state_name);

        call.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        if (response.body().getCities() != null) {
                            strings_city.clear();

                            strings_city.add("Select City");

                            for (int i = 0; i < response.body().getCities().size(); i++) {
                                strings_city.add(response.body().getCities().get(i).getCity_name());
                                Log.d("Test..1", "city_array.11." + response.body().getCities().get(i).getCity_name());
                            }

                            Log.d("Test..1", "city_array.." + strings_city);
                            city_adapter = new ArrayAdapter<String>(Registration.this, R.layout.spinnn, strings_city);
                            city_sp.setAdapter(city_adapter);
                            city_adapter.notifyDataSetChanged();

                        } else {
                            Toast.makeText(Registration.this, "cities are null", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Registration.this, "no data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Registration.this, "body null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Registration.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getRealPathFromURIPathString(Uri uri, Activity activity) {
        Log.d("joshivishal", "uri>>>> " + uri);
        cursor = activity.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            return uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void uploadRegistrationData(String filepath, String user_type, String fname, String gender, String email, String mobile, String address, String state, String city, String college, String branch, String sem, final String enrollment, final String password) {

        progressDialog = new ProgressDialog(Registration.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("user_type", user_type);
        builder.addFormDataPart("fname", fname);
        builder.addFormDataPart("gender", gender);
        builder.addFormDataPart("email", email);
        builder.addFormDataPart("mobile", mobile);
        builder.addFormDataPart("address", address);
        builder.addFormDataPart("state", state);
        builder.addFormDataPart("city", city);
        builder.addFormDataPart("college", college);
        builder.addFormDataPart("branch", branch);
        builder.addFormDataPart("sem", sem);
        builder.addFormDataPart("enrollment", enrollment);
        builder.addFormDataPart("password", password);

        Log.d("joshivishal", "filepath>>>> " + filepath);
        File file = new File(filepath);
        builder.addFormDataPart("profile_image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

        final MultipartBody requestBody = builder.build();

        Call<Result> call = service.studentRegistration(requestBody);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
//                        sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor2 = sharedPreferences_for_login.edit();
//                        editor2.putString(enroll, enrollment);
//                        editor2.putString(pass, password);
//                        editor2.putString(studID, response.body().getID().toString());
//                        editor2.commit();
                        Intent intent = new Intent(Registration.this, Login.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(Registration.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Registration.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("joshivishal", "is>>" + t.getMessage());
                Toast.makeText(Registration.this, "no response", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
