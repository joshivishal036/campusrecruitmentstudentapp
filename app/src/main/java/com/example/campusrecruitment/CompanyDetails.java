package com.example.campusrecruitment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.campusrecruitment.APIs.Api;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public class CompanyDetails extends AppCompatActivity {
    TextView name_tv,email_tv,mobile_tv,website_tv,location_tv;
    CircleImageView profile_imageview;

    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";
    SharedPreferences sharedPreferences_for_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profile_imageview=findViewById(R.id.companyDetails_profile_image);
        name_tv=findViewById(R.id.companyDetails_name);
        email_tv=findViewById(R.id.companyDetails_email);
        mobile_tv=findViewById(R.id.companyDetails_mobile);
        website_tv=findViewById(R.id.companyDetails_website);
        location_tv=findViewById(R.id.companyDetails_location);

        Glide.with(CompanyDetails.this)
                .load(Api.BASE_URL+getIntent().getStringExtra("profileimage"))
                .thumbnail(0.5f)
                .apply(RequestOptions.placeholderOf(R.drawable.profile_image)
                        .error(R.drawable.login_icon)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(profile_imageview);
        name_tv.setText(getIntent().getStringExtra("name"));
        email_tv.setText(getIntent().getStringExtra("email"));
        mobile_tv.setText(getIntent().getStringExtra("mobile"));
        website_tv.setText(getIntent().getStringExtra("website"));
        location_tv.setText(getIntent().getStringExtra("address")+","+getIntent().getStringExtra("city")+","+getIntent().getStringExtra("state"));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
