package com.example.campusrecruitment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.campusrecruitment.APIs.Api;
import com.example.campusrecruitment.APIs.ApiService;
import com.example.campusrecruitment.Adapters.SearchresultRecyclerComAdapter;
import com.example.campusrecruitment.ModelClass.SearchResultlist;
import com.example.campusrecruitment.ModelClass.SearchResultlistDetails;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragmentAvailableJobsCom extends Fragment implements SearchresultRecyclerComAdapter.SearchresultAdapterclick {
    String selected_items;
    Integer i;
    ProgressDialog progressDialog;
    TextView notavailable;
    ArrayList<SearchResultlistDetails> searchResultlistDetails_array=new ArrayList<>();
    RecyclerView recyclerView;
    SearchresultRecyclerComAdapter searchresultRecyclerComAdapter;
    public FragmentAvailableJobsCom() {
        // Required empty public constructor
    }

    public FragmentAvailableJobsCom(String selected_items) {
        // Required empty public constructor
        this.selected_items=selected_items;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =inflater.inflate(R.layout.fragment_available_jobs_com, container, false);

        recyclerView=root.findViewById(R.id.search_result_recyclerview);
        notavailable=root.findViewById(R.id.avilable_jobs_not_available);
//        selected_items=getIntent().getStringExtra("selected_items").toString();
//        Log.d("joshivishal", "isss>>>> "+selected_items);
        i=(selected_items.length())-1;
//        Toast.makeText(this, selected_items.substring(0,i), Toast.LENGTH_SHORT).show();

        set_data();

        searchresultRecyclerComAdapter =new SearchresultRecyclerComAdapter(FragmentAvailableJobsCom.this,searchResultlistDetails_array);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(searchresultRecyclerComAdapter);
        searchresultRecyclerComAdapter.notifyDataSetChanged();

        return root;
    }

    public void set_data()
    {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service=retrofit.create(ApiService.class);
        Call<SearchResultlist> call=service.getjoblists(selected_items.substring(0,i));

        call.enqueue(new Callback<SearchResultlist>() {
            @Override
            public void onResponse(Call<SearchResultlist> call, Response<SearchResultlist> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().isSuccess())
                    {
                        if (response.body().getRequests_details().isEmpty())
                        {
                            notavailable.setVisibility(View.VISIBLE);
                            notavailable.setText("Not Available...");
                        }
                        else
                        {
                            searchResultlistDetails_array.clear();
                            searchResultlistDetails_array.addAll(response.body().getRequests_details());
                            searchresultRecyclerComAdapter.notifyDataSetChanged();
                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SearchResultlist> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "failed!!", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void recyclerviewclick(int position) {

        Intent intent=new Intent(getContext(),JobDetails.class);
        intent.putExtra("cr_id",searchResultlistDetails_array.get(position).getCr_id());
//        Log.d("joshivishal", "recyclerviewclick: "+searchResultlistDetails_array.get(position).getCr_id());
        intent.putExtra("companyname",searchResultlistDetails_array.get(position).getCr_company_name());
        intent.putExtra("language",searchResultlistDetails_array.get(position).getCr_language());
        intent.putExtra("interview_date",searchResultlistDetails_array.get(position).getCr_interview_date());
        intent.putExtra("email",searchResultlistDetails_array.get(position).getCr_email());
        intent.putExtra("mobile",searchResultlistDetails_array.get(position).getCr_mobile());
        intent.putExtra("interview_place",searchResultlistDetails_array.get(position).getCr_interview_place());
        intent.putExtra("website",searchResultlistDetails_array.get(position).getCr_website());
        intent.putExtra("otherdetails",searchResultlistDetails_array.get(position).getCr_otherdetails());
        startActivity(intent);
    }
}
