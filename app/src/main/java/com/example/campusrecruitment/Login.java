package com.example.campusrecruitment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.campusrecruitment.APIs.Api;
import com.example.campusrecruitment.APIs.ApiService;
import com.example.campusrecruitment.ModelClass.Result;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity {

    EditText username_et, password_et;
    Button login_btn;
    TextView signup_tv;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences_for_reg;
    SharedPreferences sharedPreferences_for_login;
    String  myprefe2="login_shared_pre",enroll="Enroll",pass="Password", user_type = "student",studID="ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
        if (sharedPreferences_for_login.contains(enroll) && sharedPreferences_for_login.contains(pass)) {
            Intent intent = new Intent(Login.this, Student_Dashboard.class);
            startActivity(intent);
            finish();
        }

        username_et = findViewById(R.id.login_username);
        password_et = findViewById(R.id.login_password);
        login_btn = findViewById(R.id.login_login_btn);
        signup_tv = findViewById(R.id.login_tv_sign_up);

        signup_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Registration.class);
                startActivity(intent);
            }
        });
    }

    public void onclick1(View v) {

        progressDialog = new ProgressDialog(Login.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        if (username_et.getText().length() != 12) {
            username_et.setError("enter your id");
        } else if (password_et.getText().toString().equals("")) {
            password_et.setError("enter your password");
        } else {
            String enrollment = username_et.getText().toString();
            String password = password_et.getText().toString();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Api.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiService service = retrofit.create(ApiService.class);
            Call<Result> call = service.studentLogin(enrollment, password, user_type);
            call.enqueue(new Callback<Result>() {

                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    progressDialog.dismiss();
                    if (response.body() != null) {

//                        Log.w("2.0 getFeed > Full json res wrapped in gson => ", new Gson().toJson(response));
                        if (response.body().getSuccess()) {
                            sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor2 = sharedPreferences_for_login.edit();
                            editor2.putString(enroll, username_et.getText().toString());
                            editor2.putString(pass, password_et.getText().toString());
                            editor2.putString(studID,response.body().getID().toString());
                            editor2.commit();

                            Intent intent = new Intent(Login.this, Student_Dashboard.class);
                            startActivity(intent);
                            finish();

                        }
                        else
                        {
                            Toast.makeText(Login.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(Login.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    progressDialog.dismiss();
//                    Log.d("Loginsdjfh", "iss>>" + t.getMessage());
                    Toast.makeText(Login.this, "response failed", Toast.LENGTH_SHORT).show();

                }
            });
        }

    }

}


