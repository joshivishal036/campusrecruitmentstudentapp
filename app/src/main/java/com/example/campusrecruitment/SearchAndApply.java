package com.example.campusrecruitment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class SearchAndApply extends AppCompatActivity {
    Button button;
    CheckBox android_cb,ios_cb,hybrid_cb,php_cb,dotnet_cb,python_cb;
    String selected_items;

    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";
    SharedPreferences sharedPreferences_for_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_and_apply);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        button=findViewById(R.id.search_and_apply_searchbtn);
        android_cb=findViewById(R.id.search_and_apply_android_cb);
        ios_cb=findViewById(R.id.search_and_apply_ios_cb);
        hybrid_cb=findViewById(R.id.search_and_apply_hybrid_cb);
        php_cb=findViewById(R.id.search_and_apply_php_cb);
        dotnet_cb=findViewById(R.id.search_and_apply_dotnet_cb);
        python_cb=findViewById(R.id.search_and_apply_python_cb);
    }
    public void onclick(View view)
    {
        selected_items="";
        if (android_cb.isChecked())
        {
            selected_items=selected_items+android_cb.getText().toString()+",";
        }
        if (ios_cb.isChecked())
        {
            selected_items=selected_items+ios_cb.getText().toString()+",";
        }
        if (hybrid_cb.isChecked())
        {
            selected_items=selected_items+hybrid_cb.getText().toString()+",";
        }
        if (php_cb.isChecked())
        {
            selected_items=selected_items+php_cb.getText().toString()+",";
        }
        if (dotnet_cb.isChecked())
        {
            selected_items=selected_items+dotnet_cb.getText().toString()+",";
        }
        if (python_cb.isChecked())
        {
            selected_items=selected_items+python_cb.getText().toString()+",";
        }
        if (selected_items.length()<=0)
        {
            Toast.makeText(this, "please select atleast one out of this all", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Intent intent=new Intent(SearchAndApply.this, AvailableJobsTabbedActivity.class);
            intent.putExtra("selected_items",selected_items);
            startActivity(intent);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
