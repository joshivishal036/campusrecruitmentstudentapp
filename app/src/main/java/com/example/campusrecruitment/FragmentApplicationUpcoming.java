package com.example.campusrecruitment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.campusrecruitment.APIs.Api;
import com.example.campusrecruitment.APIs.ApiService;
import com.example.campusrecruitment.Adapters.UpcomingApplicationsRecyclerAdapter;
import com.example.campusrecruitment.ModelClass.StudentApplications;
import com.example.campusrecruitment.ModelClass.StudentApplicationsDetails;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragmentApplicationUpcoming extends Fragment implements UpcomingApplicationsRecyclerAdapter.ApplicationsAdapterclick {
    Context context;
    ArrayList<StudentApplicationsDetails> details = new ArrayList<>();
    TextView noavailable;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    UpcomingApplicationsRecyclerAdapter upcomingApplicationsRecyclerAdapter;
    Applicatons applicatons;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";

    public FragmentApplicationUpcoming() {
        // Required empty public constructor
    }

    public FragmentApplicationUpcoming(Applicatons applicatons, SharedPreferences sharedPreferences_for_login) {
        // Required empty public constructor
        this.context = applicatons;
        this.sharedPreferences_for_login = sharedPreferences_for_login;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_application_upcoming, container, false);

        recyclerView = root.findViewById(R.id.upcoming_applications_recyclerview_widget);
        noavailable=root.findViewById(R.id.upcoming_not_available);
        Log.d("joshivishal", "isss..>>. " + user_type + "..." + this.sharedPreferences_for_login.getString(studID, ""));
//        setdatatoadapter(user_type, sharedPreferences_for_login.getString(studID, ""));

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiService = retrofit.create(ApiService.class);

        Call<StudentApplications> call = apiService.getstudentapplications(user_type, sharedPreferences_for_login.getString(studID, ""));

        call.enqueue(new Callback<StudentApplications>() {
            @Override
            public void onResponse(Call<StudentApplications> call, Response<StudentApplications> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        if (response.body().getUpcomingstudentApplicationsDetails().isEmpty())
                        {
                            noavailable.setVisibility(View.VISIBLE);
                            noavailable.setText("there are no any Upcoming Applications");
                        }
                        else
                        {
                            details.clear();
                            details.addAll(response.body().getUpcomingstudentApplicationsDetails());
                            upcomingApplicationsRecyclerAdapter = new UpcomingApplicationsRecyclerAdapter(FragmentApplicationUpcoming.this, details);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(context));
                            recyclerView.setAdapter(upcomingApplicationsRecyclerAdapter);
                            upcomingApplicationsRecyclerAdapter.notifyDataSetChanged();
                        }

                    } else {
                        Toast.makeText(context, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "null body", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StudentApplications> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(context, "no response", Toast.LENGTH_SHORT).show();
            }
        });

        return root;
    }

    @Override
    public void recyclerviewclick(int position) {
        Intent intent = new Intent(getContext(), ApplicationDetails.class);
        intent.putExtra("stud_app_id", details.get(position).getStud_app_id());
        intent.putExtra("stud_app_stud_id", details.get(position).getStud_app_stud_id());
        intent.putExtra("stud_app_cr_id", details.get(position).getStud_app_cr_id());
        intent.putExtra("stud_app_stud_profileimage_url", details.get(position).getStud_app_stud_profileimage_url());
        intent.putExtra("stud_app_stud_name", details.get(position).getStud_app_stud_name());
        intent.putExtra("stud_app_stud_gender", details.get(position).getStud_app_stud_gender());
        intent.putExtra("stud_app_stud_email", details.get(position).getStud_app_stud_email());
        intent.putExtra("stud_app_stud_mobile", details.get(position).getStud_app_stud_mobile());
        intent.putExtra("stud_app_stud_state", details.get(position).getStud_app_stud_state());
        intent.putExtra("stud_app_stud_city", details.get(position).getStud_app_stud_city());
        intent.putExtra("stud_app_stud_clgname", details.get(position).getStud_app_stud_clgname());
        intent.putExtra("stud_app_stud_branch", details.get(position).getStud_app_stud_branch());
        intent.putExtra("stud_app_stud_sem", details.get(position).getStud_app_stud_sem());
        intent.putExtra("stud_app_stud_enrollment", details.get(position).getStud_app_stud_enrollment());
        intent.putExtra("stud_app_stud_cgpa", details.get(position).getStud_app_stud_cgpa());
        intent.putExtra("stud_app_stud_applieddate", details.get(position).getStud_app_stud_applieddate());
        intent.putExtra("stud_app_stud_photourl", details.get(position).getStud_app_stud_photourl());
        intent.putExtra("stud_app_stud_fileurl", details.get(position).getStud_app_stud_fileurl());
        intent.putExtra("stud_app_com_name", details.get(position).getStud_app_com_name());
        intent.putExtra("stud_app_com_language", details.get(position).getStud_app_com_language());
        intent.putExtra("stud_app_com_interdate", details.get(position).getStud_app_com_interdate());
        intent.putExtra("stud_app_com_email", details.get(position).getStud_app_com_email());
        intent.putExtra("stud_app_com_mobile", details.get(position).getStud_app_com_mobile());
        intent.putExtra("stud_app_com_interplace", details.get(position).getStud_app_com_interplace());
        intent.putExtra("stud_app_com_website", details.get(position).getStud_app_com_website());
        intent.putExtra("stud_app_com_otherdetails", details.get(position).getStud_app_com_otherdetails());
        startActivity(intent);
    }
}
