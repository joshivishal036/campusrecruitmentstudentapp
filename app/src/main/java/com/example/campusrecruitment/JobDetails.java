package com.example.campusrecruitment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.example.campusrecruitment.APIs.Api;
import com.example.campusrecruitment.APIs.ApiService;
import com.example.campusrecruitment.ModelClass.Result;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class JobDetails extends AppCompatActivity {
    TextView companyname_tv,lanuage_tv,interview_date_tv,email_tv,mobile_tv,place_of_interview_tv,website_tv,other_details_tv;
    ProgressDialog progressDialog;
    TextView photopath_tv,filepath_tv,photoerror_tv,fileerror_tv;
    Button photo_btn,file_btn,apply_btn;
    EditText Cgpa_et;

    Integer REQUEST_PHOTO_CODE = 200;
    Integer REQUEST_FILE_CODE = 300;

    String PhotoPath,FilePath;
    SharedPreferences sharedPreferences_for_login;

    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";
    String cr_id,companyname,language, interview_date, email, mobile, interview_place, website, otherdetails,cgpa,applied_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        companyname_tv=findViewById(R.id.job_details_company_name_tv);
        lanuage_tv=findViewById(R.id.job_details_language_tv);
        interview_date_tv=findViewById(R.id.job_details_interview_date_tv);
        email_tv=findViewById(R.id.job_details_email_tv);
        mobile_tv=findViewById(R.id.job_details_mobile_tv);
        place_of_interview_tv=findViewById(R.id.job_details_place_Interview_tv);
        website_tv=findViewById(R.id.job_details_website_tv);
        other_details_tv=findViewById(R.id.job_details_other_details_tv);

        photopath_tv=findViewById(R.id.job_details_photopath_tv);
        photoerror_tv=findViewById(R.id.job_details_photoerror_tv);
        photo_btn=findViewById(R.id.job_details_Photo_browsebtn);
        filepath_tv=findViewById(R.id.job_details_filepath_tv);
        fileerror_tv=findViewById(R.id.job_details_fileerror_tv);
        file_btn=findViewById(R.id.job_details_Resume_browsebtn);
        Cgpa_et=findViewById(R.id.job_details_CGPA_et);
        apply_btn=findViewById(R.id.job_details_apply_btn);

        companyname_tv.setText(getIntent().getStringExtra("companyname"));
        Log.d("joshivishal", "recyclerviewclick: "+getIntent().getStringExtra("cr_id"));
        sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
        Log.d("joshivishal1", "recyclerviewclick: "+sharedPreferences_for_login.getString(studID,""));
//        sharedPreferences_for_login.getString(studID,""
        lanuage_tv.setText(getIntent().getStringExtra("language"));
        interview_date_tv.setText(getIntent().getStringExtra("interview_date"));
        email_tv.setText(getIntent().getStringExtra("email"));
        mobile_tv.setText(getIntent().getStringExtra("mobile"));
        place_of_interview_tv.setText(getIntent().getStringExtra("interview_place"));
        website_tv.setText(getIntent().getStringExtra("website"));
        other_details_tv.setText(getIntent().getStringExtra("otherdetails"));

        photo_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_PHOTO_CODE);
            }
        });

        file_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent,REQUEST_FILE_CODE);
            }
        });
    }

    public void onclick(View view)
    {
        if (Cgpa_et.getText().toString().isEmpty())
        {
            Cgpa_et.setError("Enter CGPA!!");
        }
        else if (photopath_tv.getText().toString().isEmpty())
        {
            photoerror_tv.setText("Please Select Photo here!!");
        }
        else if (filepath_tv.getText().toString().isEmpty())
        {
            fileerror_tv.setText("First Select File here!!");
        }
        else
        {
            sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            cr_id=getIntent().getStringExtra("cr_id");
            companyname=getIntent().getStringExtra("companyname");
            language=getIntent().getStringExtra("language");
            interview_date=getIntent().getStringExtra("interview_date");
            email=getIntent().getStringExtra("email");
            mobile=getIntent().getStringExtra("mobile");
            interview_place=getIntent().getStringExtra("interview_place");
            website=getIntent().getStringExtra("website");
            otherdetails=getIntent().getStringExtra("otherdetails");
            cgpa=Cgpa_et.getText().toString();

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            applied_date = sdf.format(new Date());

            apply_method(sharedPreferences_for_login.getString(studID,""),PhotoPath,FilePath,cr_id,companyname,language,interview_date,email,mobile,interview_place,website,otherdetails,cgpa,applied_date);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_PHOTO_CODE&&resultCode==RESULT_OK)
        {
            Uri uri=data.getData();
//            try {
//                Bitmap bitmap= MediaStore.Images.Media.getBitmap(JobDetails.this.getContentResolver(),uri);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            PhotoPath=getRealPathFromURIPathString(uri,JobDetails.this);
            photopath_tv.setText(PhotoPath);
        }
        if (requestCode==REQUEST_FILE_CODE&&resultCode==RESULT_OK)
        {

            Uri uri=data.getData();

            FilePath=FileUtils.getPath(JobDetails.this,uri);
            filepath_tv.setText(FilePath);

        }

    }

    public String getRealPathFromURIPathString(Uri uri, Activity activity)
    {
        Cursor cursor=activity.getContentResolver().query(uri,null,null,null,null);

        if (cursor==null)
        {
            return uri.getPath();
        }
        else
        {
            cursor.moveToFirst();
            int idx=cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void apply_method(String studID,String Photopath,String Filepath,String cr_id,String companyname,String language,String interview_date,String email,String mobile,String interview_place,String website,String otherdetails,String cgpa,String applied_date)
    {
        progressDialog = new ProgressDialog(JobDetails.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RequestBody studID_1=RequestBody.create(MediaType.parse("multipart/form-data"),studID);
        RequestBody cr_id_1=RequestBody.create(MediaType.parse("multipart/form-data"),cr_id);
        RequestBody companyname_1=RequestBody.create(MediaType.parse("multipart/form-data"),companyname);
        RequestBody language_1=RequestBody.create(MediaType.parse("multipart/form-data"),language);
        RequestBody interview_date_1=RequestBody.create(MediaType.parse("multipart/form-data"),interview_date);
        RequestBody email_1=RequestBody.create(MediaType.parse("multipart/form-data"),email);
        RequestBody mobile_1=RequestBody.create(MediaType.parse("multipart/form-data"),mobile);
        RequestBody interview_place_1=RequestBody.create(MediaType.parse("multipart/form-data"),interview_place);
        RequestBody website_1=RequestBody.create(MediaType.parse("multipart/form-data"),website);
        RequestBody otherdetails_1=RequestBody.create(MediaType.parse("multipart/form-data"),otherdetails);
        RequestBody cgpa_1=RequestBody.create(MediaType.parse("multipart/form-data"),cgpa);
        RequestBody applied_date_1=RequestBody.create(MediaType.parse("multipart/form-data"),applied_date);

        File Photofile=new File(Photopath);
        RequestBody Photopath_1=RequestBody.create(MediaType.parse("multipart/form-data"),Photofile);
        MultipartBody.Part photo=MultipartBody.Part.createFormData("photo", Photofile.getName(), Photopath_1);

        File Filefile=new File(Filepath);
        RequestBody Filepath_1=RequestBody.create(MediaType.parse("multipart/form-data"),Filefile);
        MultipartBody.Part file= MultipartBody.Part.createFormData("file",Filefile.getName(),Filepath_1);

        ApiService service=retrofit.create(ApiService.class);
        Call<Result> call=service.studentapplication(studID_1,cr_id_1,companyname_1,language_1,interview_date_1,email_1,mobile_1,interview_place_1,website_1,otherdetails_1,cgpa_1,applied_date_1,photo,file);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().getSuccess())
                    {
//                        Toast.makeText(JobDetails.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(JobDetails.this, Applicatons.class);
                            startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(JobDetails.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(JobDetails.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(JobDetails.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
