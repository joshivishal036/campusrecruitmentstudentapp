package com.example.campusrecruitment.APIs;

import com.example.campusrecruitment.ModelClass.City;
import com.example.campusrecruitment.ModelClass.Companies;
import com.example.campusrecruitment.ModelClass.SearchResultlist;
import com.example.campusrecruitment.ModelClass.Result;
import com.example.campusrecruitment.ModelClass.State;
import com.example.campusrecruitment.ModelClass.Student;
import com.example.campusrecruitment.ModelClass.StudentApplications;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

    @FormUrlEncoded
    @POST("login.php")
    Call<Result> studentLogin(
            @Field("enrollment") String enrollment,
            @Field("password") String password,
            @Field("user_type") String user_type
    );

    @FormUrlEncoded
    @POST("state.php")
    Call<State> getstate(
            @Field("user_type") String user_type
    );

    @FormUrlEncoded
    @POST("city.php")
    Call<City> getcity(
            @Field("S") String S
    );

//    @FormUrlEncoded
//    @POST("college.php")
//    Call<College> getcollege(
//            @Field("user_type") String usertype
//    );
//
//    @FormUrlEncoded
//    @POST("branch.php")
//    Call<Branch> getbranch(
//            @Field("user_type") String usertype
//    );

//    @FormUrlEncoded
//    @POST("semester.php")
//    Call<Semester> getsemester(
//            @Field("user_type") String usertype
//    );

//    @FormUrlEncoded
//    @POST("Registration.php")
//    Call<Result> studentRegistration(
//            @Field("user_type") String user_type,
//            @Field("fname") String full_name,
//            @Field("gender") String gender,
//            @Field("email") String email,
//            @Field("mobile") String mobile,
//            @Field("address") String address,
//            @Field("state") String state,
//            @Field("city") String city,
//            @Field("college") String college,
//            @Field("branch") String branch,
//            @Field("sem") String sem,
//            @Field("enroll") String enroll,
//            @Field("password") String password
//    );


    @POST("Registration.php")
    Call<Result> studentRegistration(@Body RequestBody profile_image);

//    @POST("edit_profile")
//    Call<Result> edit_profile_pic(
//            @Body RequestBody file);

    @FormUrlEncoded
    @POST("getprofile.php")
    Call<Student> studentdetails(
            @Field("user_type") String user_type,
            @Field("studID") String studID

    );

    @Multipart
    @POST("updateprofile.php")
    Call<Result> updateprofile(@Part("user_type") RequestBody user_type,
                               @Part("studID") RequestBody studID,
                               @Part("fname") RequestBody fname,
                               @Part("gender") RequestBody gender,
                               @Part("email") RequestBody email,
                               @Part("mobile") RequestBody mobile,
                               @Part("address") RequestBody address,
                               @Part("state") RequestBody state,
                               @Part("city") RequestBody city,
                               @Part("college") RequestBody college,
                               @Part("branch") RequestBody branch,
                               @Part("sem") RequestBody sem,
                               @Part("enroll") RequestBody enroll,
                               @Part MultipartBody.Part profile_image
    );


    @FormUrlEncoded
    @POST("changepassword.php")
    Call<Result> changepassword(
            @Field("user_type") String user_type,
            @Field("studID") String studID,
            @Field("oldpass") String oldpass,
            @Field("newpass") String newpass

    );

    @FormUrlEncoded
    @POST("getcompanies.php")
    Call<Companies> getcompanies(
            @Field("string") String string
    );

    @FormUrlEncoded
    @POST("getjoblists.php")
    Call<SearchResultlist> getjoblists(
            @Field("string") String string
    );

    @FormUrlEncoded
    @POST("getjoblistscoll.php")
    Call<SearchResultlist> getjoblistscoll(
            @Field("string") String string
    );

    @Multipart
    @POST("studentApplication.php")
     Call<Result> studentapplication(
             @Part("studID") RequestBody studID,
             @Part("cr_id") RequestBody cr_id,
             @Part("comname") RequestBody comname,
             @Part("language") RequestBody language,
             @Part("interview_date") RequestBody interview_date,
             @Part("email") RequestBody email,
             @Part("mobile") RequestBody mobile,
             @Part("interview_place") RequestBody interview_place,
             @Part("website") RequestBody website,
             @Part("otherdetails") RequestBody otherdetails,
             @Part("cgpa") RequestBody cgpa,
             @Part("applied_date") RequestBody applied_date,
             @Part MultipartBody.Part photo,
             @Part MultipartBody.Part file
    );

    @FormUrlEncoded
    @POST("getstudentapplication.php")
    Call<StudentApplications> getstudentapplications(
            @Field("user_type") String user_type,
            @Field("studID") String studID
    );

}
