package com.example.campusrecruitment.Adapters;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.campusrecruitment.Applicatons;
import com.example.campusrecruitment.FragmentApplicationOutdated;
import com.example.campusrecruitment.FragmentApplicationUpcoming;

public class ApplicationTabViewAdapter extends FragmentPagerAdapter {
    private Context context;
    public int tabcount;
    SharedPreferences sharedPreferences_for_login;
    Applicatons applicatons;
    public ApplicationTabViewAdapter(Applicatons applicatons, @NonNull FragmentManager fm, int tabcount, SharedPreferences sharedPreferences_for_login) {
        super(fm);
        this.context= applicatons;
        this.tabcount=tabcount;
        this.sharedPreferences_for_login=sharedPreferences_for_login;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                FragmentApplicationUpcoming fragmentApplicationUpcoming=new FragmentApplicationUpcoming(applicatons,this.sharedPreferences_for_login);
                return fragmentApplicationUpcoming;

            case 1:
                FragmentApplicationOutdated fragmentApplicationOutdated=new FragmentApplicationOutdated(applicatons,this.sharedPreferences_for_login);
                return fragmentApplicationOutdated;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
