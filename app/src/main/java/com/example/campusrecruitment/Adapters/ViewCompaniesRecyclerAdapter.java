package com.example.campusrecruitment.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.campusrecruitment.APIs.Api;
import com.example.campusrecruitment.ModelClass.DetailsCompany;
import com.example.campusrecruitment.R;
import com.example.campusrecruitment.Student_Dashboard;
import com.example.campusrecruitment.Viewcompanies;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewCompaniesRecyclerAdapter extends RecyclerView.Adapter<ViewCompaniesRecyclerAdapter.ViewHolder> {
//    ArrayList<Integer> icon_image;
    ArrayList<DetailsCompany> companies;
    Context context;
    ViewCompaniesAdapterclick adapterclick;
//    Student_Dashboard student_dashboard;


    public ViewCompaniesRecyclerAdapter(Viewcompanies viewcompanies, ArrayList<DetailsCompany> companies) {
        this.context=viewcompanies;
//        this.icon_image = icon_image;
        this.companies = companies;

        try
        {
            this.adapterclick=((ViewCompaniesAdapterclick)viewcompanies);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.view_companies_recyclerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.companyname_tv.setText(companies.get(position).getCompany_name());
        holder.mail_tv.setText(companies.get(position).getCompany_email());
        holder.city_tv.setText(companies.get(position).getCompany_city());
        Glide.with(context)
                .load(Api.BASE_URL+companies.get(position).getCompany_profileimage_url())
                .thumbnail(0.5f)
                .apply(RequestOptions.placeholderOf(R.drawable.profile_image)
                        .error(R.drawable.login_icon)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(holder.company_profile_imageview);

//        holder.imageView.setImageResource(icon_image.get(position));
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView companyname_tv;
        public TextView mail_tv;
        public TextView city_tv;
        public CircleImageView company_profile_imageview;
//        public ImageView imageView;

        public ViewHolder(View list) {
            super(list);
//            this.imageView=list.findViewById(R.id.stud_dash_recyclerlist_icon_image);
            this.companyname_tv=list.findViewById(R.id.view_company_recyclerlist_companyname_tv);
            this.mail_tv=list.findViewById(R.id.view_company_recyclerlist_mail_tv);
            this.city_tv=list.findViewById(R.id.view_company_recyclerlist_city_tv);
            this.company_profile_imageview=list.findViewById(R.id.view_companies_profile_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }
    public interface ViewCompaniesAdapterclick
    {
        public void recyclerviewclick(int position);
    }
}

