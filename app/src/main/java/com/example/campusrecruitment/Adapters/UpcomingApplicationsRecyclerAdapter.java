package com.example.campusrecruitment.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.campusrecruitment.FragmentApplicationUpcoming;
import com.example.campusrecruitment.ModelClass.StudentApplicationsDetails;
import com.example.campusrecruitment.R;

import java.util.ArrayList;

public class UpcomingApplicationsRecyclerAdapter extends RecyclerView.Adapter<UpcomingApplicationsRecyclerAdapter.ViewHolder> {
//    ArrayList<Integer> icon_image;
    ArrayList<StudentApplicationsDetails> details;
    FragmentApplicationUpcoming fragmentApplicationUpcoming;
    ApplicationsAdapterclick adapterclick;
//    Student_Dashboard student_dashboard;


    public UpcomingApplicationsRecyclerAdapter(FragmentApplicationUpcoming fragmentApplicationUpcoming, ArrayList<StudentApplicationsDetails> details) {
//        this.context=applications;
//        this.icon_image = icon_image;
        this.fragmentApplicationUpcoming= fragmentApplicationUpcoming;
        this.details=details;

        try
        {
            this.adapterclick=((ApplicationsAdapterclick) fragmentApplicationUpcoming);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.stud_application_recyclerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.inter_date_tv.setText(details.get(position).getStud_app_com_interdate());
        holder.companyname_tv.setText(details.get(position).getStud_app_com_name());
        holder.language_tv.setText(details.get(position).getStud_app_com_language());
        holder.req_date_tv.setText(details.get(position).getStud_app_stud_applieddate());
//        holder.imageView.setImageResource(icon_image.get(position));
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView inter_date_tv;
        public TextView companyname_tv;
        public TextView language_tv;
        public TextView req_date_tv;
//        public ImageView imageView;

        public ViewHolder(View list) {
            super(list);

            this.inter_date_tv=list.findViewById(R.id.stud_app_interviewdate_tv);
            this.companyname_tv=list.findViewById(R.id.stud_app_cname_tv);
            this.language_tv=list.findViewById(R.id.stud_app_language_tv);
            this.req_date_tv=list.findViewById(R.id.stud_app_requesteddate_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }
    public interface ApplicationsAdapterclick
    {
        public void recyclerviewclick(int position);
    }
}

