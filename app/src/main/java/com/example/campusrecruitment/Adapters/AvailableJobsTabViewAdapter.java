package com.example.campusrecruitment.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.campusrecruitment.FragmentAvailableJobsColl;
import com.example.campusrecruitment.FragmentAvailableJobsCom;

public class AvailableJobsTabViewAdapter extends FragmentPagerAdapter {
    String selected_items;
    public int tabcount;

    public AvailableJobsTabViewAdapter( @NonNull FragmentManager fm, int tabcount,String selected_items) {
        super(fm);
        this.tabcount=tabcount;
        this.selected_items=selected_items;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                FragmentAvailableJobsCom fragmentAvailableJobsCom=new FragmentAvailableJobsCom(selected_items);
                return fragmentAvailableJobsCom;

            case 1:
                FragmentAvailableJobsColl fragmentAvailableJobsColl=new FragmentAvailableJobsColl(selected_items);
                return fragmentAvailableJobsColl;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
