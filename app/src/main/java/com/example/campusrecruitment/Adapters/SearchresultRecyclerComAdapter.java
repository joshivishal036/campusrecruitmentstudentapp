package com.example.campusrecruitment.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.campusrecruitment.FragmentAvailableJobsCom;
import com.example.campusrecruitment.ModelClass.SearchResultlistDetails;
import com.example.campusrecruitment.R;

import java.util.ArrayList;

public class SearchresultRecyclerComAdapter extends RecyclerView.Adapter<SearchresultRecyclerComAdapter.ViewHolder> {

    ArrayList<SearchResultlistDetails> details;
    FragmentAvailableJobsCom fragmentAvailableJobsCom;
    SearchresultAdapterclick adapterclick;


    public SearchresultRecyclerComAdapter(FragmentAvailableJobsCom fragmentAvailableJobsCom, ArrayList<SearchResultlistDetails> details) {
        this.fragmentAvailableJobsCom=fragmentAvailableJobsCom;

        this.details = details;

        try
        {
            this.adapterclick=((SearchresultAdapterclick)fragmentAvailableJobsCom);
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.stud_search_result_recyclerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.companyname_tv.setText(details.get(position).getCr_company_name());
        holder.language_tv.setText(details.get(position).getCr_language());
        holder.inter_date_tv.setText(details.get(position).getCr_interview_date());
        holder.place_tv.setText(details.get(position).getCr_interview_place());

//        holder.imageView.setImageResource(icon_image.get(position));
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView companyname_tv;
        public TextView language_tv;
        public TextView inter_date_tv;
        public TextView place_tv;
//        public ImageView imageView;

        public ViewHolder(View list) {
            super(list);
//            this.imageView=list.findViewById(R.id.stud_dash_recyclerlist_icon_image);
            this.companyname_tv=list.findViewById(R.id.stud_searchresult_cname_tv);
            this.language_tv=list.findViewById(R.id.stud_searchresult_language_tv);
            this.inter_date_tv=list.findViewById(R.id.stud_searchresult_interviewdate_tv);
            this.place_tv=list.findViewById(R.id.stud_searchresult_place_tv);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }

            });
            }
    }
    public interface SearchresultAdapterclick
    {
        public void recyclerviewclick(int position);
    }
}

