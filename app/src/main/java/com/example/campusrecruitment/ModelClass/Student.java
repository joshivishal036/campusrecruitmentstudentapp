package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Student {

    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("studentdetails")
    private ArrayList<Studentdetail> studentdetails;

    public Student(boolean success, String msg, ArrayList<Studentdetail> studentdetails) {
        this.success = success;
        this.msg = msg;
        this.studentdetails = studentdetails;
    }

    public boolean GetSuccess() {
        return success;
    }

    public String GetMsg() {
        return msg;
    }

    public ArrayList<Studentdetail> getStudentdetails() {
        return studentdetails;
    }
}
