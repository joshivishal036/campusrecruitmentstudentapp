package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class State {

    @SerializedName("success")
    private boolean success;

    @SerializedName("states")
    private ArrayList<States> states;

    @SerializedName("colleges")
    private ArrayList<States> colleges;

    @SerializedName("branches")
    private ArrayList<States> branches;

    @SerializedName("semesters")
    private ArrayList<States> semesters;

    public State(boolean success, ArrayList<States> states, ArrayList<States> colleges, ArrayList<States> branches, ArrayList<States> semesters) {
        this.success = success;
        this.states = states;
        this.colleges = colleges;
        this.branches = branches;
        this.semesters = semesters;
    }

    public boolean isSuccess() {
        return success;
    }

    public ArrayList<States> getStates() {
        return states;
    }

    public ArrayList<States> getColleges() {
        return colleges;
    }

    public ArrayList<States> getBranches() {
        return branches;
    }

    public ArrayList<States> getSemesters() {
        return semesters;
    }
}
