package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("success")// to access the php side json_encod['success'].
    private Boolean success;

    @SerializedName("msg")// to access the php side json_encod['success'].
    private String msg;

    @SerializedName("ID")// to access the php side json_encod['success'].
    private Integer ID;

    public Result(Boolean success, String msg, Integer ID) {
        this.success = success;
        this.msg = msg;
        this.ID = ID;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public Integer getID() {
        return ID;
    }
}
