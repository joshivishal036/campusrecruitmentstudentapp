package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

public class SearchResultlistDetails {

    @SerializedName("cr_id")
    private String cr_id;

    @SerializedName("c_id")
    private String c_id;

    @SerializedName("cr_company_name")
    private String cr_company_name;

    @SerializedName("cr_interview_place")
    private String cr_interview_place;

    @SerializedName("cr_requested_date")
    private String cr_requested_date;

    @SerializedName("cr_interview_date")
    private String cr_interview_date;

    @SerializedName("cr_email")
    private String cr_email;

    @SerializedName("cr_mobile")
    private String cr_mobile;

    @SerializedName("cr_language")
    private String cr_language;

    @SerializedName("cr_website")
    private String cr_website;

    @SerializedName("cr_otherdetails")
    private String cr_otherdetails;

    @SerializedName("cor_id")
    private String cor_id;

    @SerializedName("cor_company_name")
    private String cor_company_name;

    @SerializedName("cor_interview_place")
    private String cor_interview_place;

    @SerializedName("cor_requested_date")
    private String cor_requested_date;

    @SerializedName("cor_interview_date")
    private String cor_interview_date;

    @SerializedName("cor_email")
    private String cor_email;

    @SerializedName("cor_mobile")
    private String cor_mobile;

    @SerializedName("cor_language")
    private String cor_language;

    @SerializedName("cor_website")
    private String cor_website;

    @SerializedName("cor_googleformlink")
    private String cor_googleformlink;

    @SerializedName("cor_otherdetails")
    private String cor_otherdetails;

    public SearchResultlistDetails(String cr_id, String c_id, String cr_company_name, String cr_interview_place, String cr_requested_date, String cr_interview_date, String cr_email, String cr_mobile, String cr_language, String cr_website, String cr_otherdetails, String cor_id, String cor_company_name, String cor_interview_place, String cor_requested_date, String cor_interview_date, String cor_email, String cor_mobile, String cor_language, String cor_website, String cor_googleformlink, String cor_otherdetails) {
        this.cr_id = cr_id;
        this.c_id = c_id;
        this.cr_company_name = cr_company_name;
        this.cr_interview_place = cr_interview_place;
        this.cr_requested_date = cr_requested_date;
        this.cr_interview_date = cr_interview_date;
        this.cr_email = cr_email;
        this.cr_mobile = cr_mobile;
        this.cr_language = cr_language;
        this.cr_website = cr_website;
        this.cr_otherdetails = cr_otherdetails;
        this.cor_id = cor_id;
        this.cor_company_name = cor_company_name;
        this.cor_interview_place = cor_interview_place;
        this.cor_requested_date = cor_requested_date;
        this.cor_interview_date = cor_interview_date;
        this.cor_email = cor_email;
        this.cor_mobile = cor_mobile;
        this.cor_language = cor_language;
        this.cor_website = cor_website;
        this.cor_googleformlink = cor_googleformlink;
        this.cor_otherdetails = cor_otherdetails;
    }

    public String getCr_id() {
        return cr_id;
    }

    public String getC_id() {
        return c_id;
    }

    public String getCr_company_name() {
        return cr_company_name;
    }

    public String getCr_interview_place() {
        return cr_interview_place;
    }

    public String getCr_requested_date() {
        return cr_requested_date;
    }

    public String getCr_interview_date() {
        return cr_interview_date;
    }

    public String getCr_email() {
        return cr_email;
    }

    public String getCr_mobile() {
        return cr_mobile;
    }

    public String getCr_language() {
        return cr_language;
    }

    public String getCr_website() {
        return cr_website;
    }

    public String getCr_otherdetails() {
        return cr_otherdetails;
    }

    public String getCor_id() {
        return cor_id;
    }

    public String getCor_company_name() {
        return cor_company_name;
    }

    public String getCor_interview_place() {
        return cor_interview_place;
    }

    public String getCor_requested_date() {
        return cor_requested_date;
    }

    public String getCor_interview_date() {
        return cor_interview_date;
    }

    public String getCor_email() {
        return cor_email;
    }

    public String getCor_mobile() {
        return cor_mobile;
    }

    public String getCor_language() {
        return cor_language;
    }

    public String getCor_website() {
        return cor_website;
    }

    public String getCor_googleformlink() {
        return cor_googleformlink;
    }

    public String getCor_otherdetails() {
        return cor_otherdetails;
    }
}
