package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StudentApplications {
    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("upcoming_applications")
    ArrayList<StudentApplicationsDetails> upcomingstudentApplicationsDetails;

    @SerializedName("outdated_applications")
    ArrayList<StudentApplicationsDetails> outdatedstudentApplicationsDetails;

    public StudentApplications(boolean success, String msg, ArrayList<StudentApplicationsDetails> upcomingstudentApplicationsDetails, ArrayList<StudentApplicationsDetails> outdatedstudentApplicationsDetails) {
        this.success = success;
        this.msg = msg;
        this.upcomingstudentApplicationsDetails = upcomingstudentApplicationsDetails;
        this.outdatedstudentApplicationsDetails = outdatedstudentApplicationsDetails;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<StudentApplicationsDetails> getUpcomingstudentApplicationsDetails() {
        return upcomingstudentApplicationsDetails;
    }

    public ArrayList<StudentApplicationsDetails> getOutdatedstudentApplicationsDetails() {
        return outdatedstudentApplicationsDetails;
    }
}
