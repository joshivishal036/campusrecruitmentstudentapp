package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

public class States {

    @SerializedName("state_name")
    private String state_name;

    @SerializedName("college_name")
    private String college_name;

    @SerializedName("branch_name")
    private String branch_name;

    @SerializedName("semester_num")
    private String semester_num;

    public States(String state_name, String college_name, String branch_name, String semester_num) {
        this.state_name = state_name;
        this.college_name = college_name;
        this.branch_name = branch_name;
        this.semester_num = semester_num;
    }

    public String getState_name() {
        return state_name;
    }

    public String getCollege_name() {
        return college_name;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public String getSemester_num() {
        return semester_num;
    }
}
