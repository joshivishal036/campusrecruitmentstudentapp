package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchResultlist {

    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("requests_details")
    private ArrayList<SearchResultlistDetails> requests_details;

    @SerializedName("coll_requests_details")
    private ArrayList<SearchResultlistDetails> coll_requests_details;

    public SearchResultlist(boolean success, String msg, ArrayList<SearchResultlistDetails> requests_details, ArrayList<SearchResultlistDetails> coll_requests_details) {
        this.success = success;
        this.msg = msg;
        this.requests_details = requests_details;
        this.coll_requests_details=coll_requests_details;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<SearchResultlistDetails> getRequests_details() {
        return requests_details;
    }

    public ArrayList<SearchResultlistDetails> getColl_requests_details() {
        return coll_requests_details;
    }
}
