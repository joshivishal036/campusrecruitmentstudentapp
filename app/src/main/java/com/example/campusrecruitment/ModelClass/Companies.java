package com.example.campusrecruitment.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Companies {

    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("approved_company_details")
    private ArrayList<DetailsCompany> company_details;

    public Companies(boolean success, String msg, ArrayList<DetailsCompany> company_details) {
        this.success = success;
        this.msg = msg;
        this.company_details = company_details;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<DetailsCompany> getCompany_details() {
        return company_details;
    }
}
