package com.example.campusrecruitment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.campusrecruitment.APIs.Api;
import com.example.campusrecruitment.APIs.ApiService;
import com.example.campusrecruitment.Adapters.ViewCompaniesRecyclerAdapter;
import com.example.campusrecruitment.ModelClass.Companies;
import com.example.campusrecruitment.ModelClass.DetailsCompany;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Viewcompanies extends AppCompatActivity implements ViewCompaniesRecyclerAdapter.ViewCompaniesAdapterclick {
    ArrayList<DetailsCompany> companies=new ArrayList<>();
    RecyclerView recyclerView;
    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";
    SharedPreferences sharedPreferences_for_login;
    ViewCompaniesRecyclerAdapter viewCompaniesRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcompanies);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView=findViewById(R.id.view_companies_recyclerview);

        final ProgressDialog progressDialog = new ProgressDialog(Viewcompanies.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service=retrofit.create(ApiService.class);

        Call<Companies> call=service.getcompanies("jayshreeram");

        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().isSuccess())
                    {
                        companies.clear();
                        companies.addAll(response.body().getCompany_details());
                        viewCompaniesRecyclerAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(Viewcompanies.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(Viewcompanies.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Viewcompanies.this, "failed", Toast.LENGTH_SHORT).show();

            }
        });
        viewCompaniesRecyclerAdapter=new ViewCompaniesRecyclerAdapter(Viewcompanies.this,companies);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(viewCompaniesRecyclerAdapter);
        viewCompaniesRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void recyclerviewclick(int position) {
        //Toast.makeText(this, "position is"+position, Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(Viewcompanies.this,CompanyDetails.class);
        intent.putExtra("profileimage", companies.get(position).getCompany_profileimage_url());
        intent.putExtra("name", companies.get(position).getCompany_name());
        intent.putExtra("email", companies.get(position).getCompany_email());
        intent.putExtra("mobile", companies.get(position).getCompany_mobile());
        intent.putExtra("website", companies.get(position).getCompany_website());
        intent.putExtra("address", companies.get(position).getCompany_address());
        intent.putExtra("state", companies.get(position).getCompany_state());
        intent.putExtra("city", companies.get(position).getCompany_city());
        Log.d("joshivishal", "isss>>> "+companies.get(position).getCompany_name()+companies.get(position).getCompany_email()+companies.get(position).getCompany_mobile()+ companies.get(position).getCompany_website()+ companies.get(position).getCompany_address()+companies.get(position).getCompany_state()+companies.get(position).getCompany_city());
        startActivity(intent);

    }

}
