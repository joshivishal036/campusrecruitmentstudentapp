package com.example.campusrecruitment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class JobDetailsCollActivity extends AppCompatActivity {
    TextView companyname_tv,lanuage_tv,interview_date_tv,email_tv,mobile_tv,place_of_interview_tv,website_tv,googleformlink_tv,other_details_tv;

    String myprefe2 = "login_shared_pre", enroll = "Enroll", pass = "Password", user_type = "student", studID = "ID";
    SharedPreferences sharedPreferences_for_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details_coll);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        companyname_tv=findViewById(R.id.job_details_coll_company_name_tv);
        lanuage_tv=findViewById(R.id.job_details_coll_language_tv);
        interview_date_tv=findViewById(R.id.job_details_coll_interview_date_tv);
        email_tv=findViewById(R.id.job_details_coll_email_tv);
        mobile_tv=findViewById(R.id.job_details_coll_mobile_tv);
        place_of_interview_tv=findViewById(R.id.job_details_coll_place_Interview_tv);
        website_tv=findViewById(R.id.job_details_coll_website_tv);
        googleformlink_tv=findViewById(R.id.job_details_coll_googleformlink_tv);
        other_details_tv=findViewById(R.id.job_details_coll_other_details_tv);

        companyname_tv.setText(getIntent().getStringExtra("companyname"));
        lanuage_tv.setText(getIntent().getStringExtra("language"));
        interview_date_tv.setText(getIntent().getStringExtra("interview_date"));
        email_tv.setText(getIntent().getStringExtra("email"));
        mobile_tv.setText(getIntent().getStringExtra("mobile"));
        place_of_interview_tv.setText(getIntent().getStringExtra("interview_place"));
        website_tv.setText(getIntent().getStringExtra("website"));
        googleformlink_tv.setText(getIntent().getStringExtra("googleformlink"));
        other_details_tv.setText(getIntent().getStringExtra("otherdetails"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
